//
//  Quiz.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 21/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit
import LTHRadioButton
class Quiz: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        rd.tintColor = UIColor.init(red: 211, green: 211, blue: 211, alpha: 0.10)
        rd.alpha = 0.50
    }

    @IBOutlet weak var rd: LTHRadioButton!
    @IBOutlet weak var sel: KGRadioButton!
    @IBOutlet weak var lb: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        rd.selectedColor = .orange
        
        
        if selected {
            return rd.select(animated: animated)
            
        }
        
        rd.deselect(animated: animated)
    }
    
}
