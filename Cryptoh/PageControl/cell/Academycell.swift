//
//  Academycell.swift
//  Cryptoh
//
//  Created by Christian on 09/06/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit
import LTHRadioButton
class Academycell: UITableViewCell {
    @IBOutlet weak var risultati: UILabel!
    @IBOutlet weak var livello: UILabel!
    
    @IBOutlet weak var rd: LTHRadioButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        rd.tintColor = UIColor.init(red: 211, green: 211, blue: 211, alpha: 0.10)
        rd.alpha = 0.50
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        rd.selectedColor = .orange
        
        
        if selected {
            return rd.select(animated: animated)
            
        }
        
        rd.deselect(animated: animated)
    }
    
    
}
