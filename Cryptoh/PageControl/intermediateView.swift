//
//  intermediateView.swift
//  Cryptoh
//
//  Created by Christian on 09/06/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit
import FlexibleSteppedProgressBar
class intermediateView: UIViewController,UITableViewDelegate,UITableViewDataSource,FlexibleSteppedProgressBarDelegate {
    @IBOutlet weak var flex: FlexibleSteppedProgressBar!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    @IBAction func btn(_ sender: Any) {
        if selezionato != 3{
            performSegue(withIdentifier: "Apri", sender: self)
        }else{
            performSegue(withIdentifier: "Finale", sender: self)
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! Academycell
        cell.livello.text = "Livello \(indexPath.row+1)"
        if cont != 0{
            cont-=1
            cell.isUserInteractionEnabled = true
            cell.backgroundColor = .white
            cell.livello.textColor = .black
            
            flex.currentIndex = index
            index+=1
        }else{
            cell.isUserInteractionEnabled = false
           
            cell.livello.textColor = .systemGray2
            cell.rd.tintColor = .systemGray5
        }
        if indexPath.row == 3{
            cell.livello.text = "Final Test"
            if Methods.shared.quizFinale[1] != 0{
                cell.isUserInteractionEnabled = true
                cell.livello.textColor = .black
                cell.rd.tintColor = .black
            }
            //flex.currentIndex = index
        }
        if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "btn") as! btnImg
            cell.isUserInteractionEnabled = true
            cell.selectionStyle = .none
        cell.id = 1
            return cell
        }
        
        
        return cell
    }
    func apri(){
        EventManager.shared.listenTo(eventName: "csv1") {
            self.btn.sendActions(for: .touchUpInside)
        }
    }
     var selezionato = 0
var cont : Int = 0
    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        checlvl()
        super.viewDidLoad()
        table.delegate = self
        table.dataSource = self
        table.allowsMultipleSelection = false
        table.register(UINib(nibName: "Academycell", bundle: nil), forCellReuseIdentifier: "cell")
        aggiorna()
        table.register(UINib(nibName: "btnImg", bundle: nil), forCellReuseIdentifier: "btn")
        table.isScrollEnabled = false
        btn.layer.cornerRadius = 7.0
        // Do any additional setup after loading the view.
        flex.numberOfPoints = 4
        flex.lineHeight = 9
        flex.radius = 15
        flex.currentIndex = 0
        flex.progressRadius = 25
        flex.currentSelectedCenterColor = .orange
        flex.progressLineHeight = 3
        flex.delegate = self
        chiudi()
        azzera()
        btn.isEnabled = false
        apri()
    }
    func azzera(){
        EventManager.shared.listenTo(eventName: "azzera") {
            self.index = 0
        }
    }
    @IBOutlet weak var btn: UIButton!
    func checlvl(){
        if Methods.shared.bia == 1{
            switch Methods.shared.arch {
            case 0:
                cont = 1
                break
            case 1 :
                cont = 2
                
            case 2 :
                cont = 3
            default:
                break
            }
        }else if Methods.shared.bia >= 1  {
            cont = 3
        }
    }
    func aggiorna(){
        EventManager.shared.listenTo(eventName: "Vinto") {
            self.checlvl()
            self.table.reloadData()
        }
    }
    var index = 0
 func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if indexPath.row != 4{
        tableView.cellForRow(at: indexPath)?.selectionStyle = .none
        btn.isEnabled = true
        selezionato = indexPath.row
        Methods.shared.sezione = 1
    }
   
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nextViewController = segue.destination as? AcademyQuiz
        {
            nextViewController.bia = 1
            nextViewController.arch = selezionato
            nextViewController.index = selezionato
            nextViewController.livello1 = "Intermediate level \(selezionato+1)"
        }
    }
    @IBAction func close(_ sender: Any) {
        let alertDemo = Popup()
        alertDemo.show(above: self, completion: nil)
    }
    func chiudi(){
        EventManager.shared.listenTo(eventName: "chiudituttcos") {
            self.dismiss(animated: false, completion: nil)
        }
    }
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     textAtIndex index: Int, position: FlexibleSteppedProgressBarTextLocation) -> String {
        return ""
    }
    override func viewWillAppear(_ animated: Bool) {
        index = 0
    }

}
