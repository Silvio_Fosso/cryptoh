//
//  TableViewCell1.swift
//  Profilo
//
//  Created by Silvio Fosso on 15/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit
import FlexibleSteppedProgressBar
class TableViewCell1: UITableViewCell, FlexibleSteppedProgressBarDelegate {
    @IBOutlet weak var adv: UILabel!
    @IBOutlet weak var beginner: UILabel!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var interm: UILabel!
    var progressBar1 = FlexibleSteppedProgressBar()
    var progressBar2 = FlexibleSteppedProgressBar()
    var progressBar3 = FlexibleSteppedProgressBar()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        inizializeProgressBar(progressBar: progressBar1, men: 70)
        inizializeProgressBar(progressBar: progressBar2, men: 180)
        inizializeProgressBar(progressBar: progressBar3, men: 280)
    }
    
    func inizializeProgressBar(progressBar : FlexibleSteppedProgressBar,men : CGFloat){
        
        progressBar.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(progressBar)
        
        
        let horizontalConstraint = progressBar.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let verticalConstraint = progressBar.topAnchor.constraint(
            equalTo: view.topAnchor,
            constant: men
        )
        let widthConstraint = progressBar.widthAnchor.constraint(equalToConstant: 350)
        let heightConstraint = progressBar.heightAnchor.constraint(equalToConstant: 20)
        // let widthConstraint = progressBar.widthAnchor.constraintEqualToAnchor(nil, constant: 500)
        // let heightConstraint = progressBar.heightAnchor.constraintEqualToAnchor(nil, constant: 150)
        NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint,widthConstraint,heightConstraint])
        
        // Customise the progress bar here
        progressBar.numberOfPoints = 3
        progressBar.lineHeight = 9
        progressBar.radius = 15
        progressBar.progressRadius = 25
        
        progressBar.currentSelectedCenterColor = .orange
        progressBar.progressLineHeight = 3
        progressBar.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     textAtIndex index: Int, position: FlexibleSteppedProgressBarTextLocation) -> String {
        return ""
    }
   

  
    
}
