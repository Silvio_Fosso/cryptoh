//
//  Invistimento_p.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 23/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit

class Invistimento_p: UITableViewCell {

    @IBOutlet weak var btn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        btn.layer.cornerRadius = 7.0
        // Configure the view for the selected state
    }
    
    @IBAction func btn(_ sender: Any) {
        EventManager.shared.trigger(eventName: "Money")
    }
}
