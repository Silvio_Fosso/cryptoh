//
//  Time.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 23/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit
import iOSDropDown
import SwiftPopup
class Time: UITableViewCell, UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arr.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arr[row]
    }
    

    @IBOutlet weak var btn: UIButton!
    var testo = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btn.layer.cornerRadius = 7.0
        btn.isEnabled = false
    }
    @IBOutlet weak var picker: UIPickerView!
    
    @IBOutlet weak var dropdown: DropDown!
    var arr = [String]()
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        picker.delegate = self
        picker.dataSource = self
        arr = ["30 Minutes", "1 Hour", "8 Hours","1 Day"]
        self.btn.isEnabled = true
        self.testo = "30 Minutes"
        // Configure the view for the selected state
       
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.testo = arr[row]
        self.btn.isEnabled = true
    }
   
    @IBAction func btn(_ sender: Any) {
        Methods.shared.orario = testo
        Methods.shared.flag1 = true
        if Methods.shared.CambiaReload == false{
          EventManager.shared.trigger(eventName: "cambia1")
            Methods.shared.CambiaReload = true
            
        }else{
           EventManager.shared.trigger(eventName: "cambia2")
        }
        
        EventManager.shared.trigger(eventName: "Close")
        
    }
}
