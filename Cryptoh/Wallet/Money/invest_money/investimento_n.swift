//
//  investimento_n.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 23/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit

class investimento_n: UITableViewCell, UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return pickerData[row]
    }

    @IBOutlet weak var btn: UIButton!
    var euro : Float!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      
    }
    
    @IBOutlet weak var picker: UIPickerView!
    
    @IBOutlet weak var testo: UITextField!
    
    
    @IBAction func Lbl(_ sender: UITextField) {
     
    }
    
    @IBAction func change(_ sender: Any) {
        Methods.shared.flag = true
        
        EventManager.shared.trigger(eventName: "cambia")
    }
       var pickerData: [String] = [String]()
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        btn.layer.cornerRadius = 7.0
        btn.isEnabled = true
        // Configure the view for the selected state
        picker.delegate = self
        picker.dataSource = self
        Methods.shared.soldi = "50"
        let soldi = (Methods.shared.getEuro() as NSString).integerValue
        print(soldi)
        if 50 > soldi {
            btn.isEnabled = false
        }
        pickerData = ["50", "100", "200", "500", "1000"]
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
     let soldi = (Methods.shared.getEuro()as NSString).integerValue
    let scelto = Int(pickerData[row])!
        if scelto > soldi {
            btn.isEnabled = false
        }else{
          btn.isEnabled = true
        }
       Methods.shared.soldi = pickerData[row]
    }
    
    
}
extension String {
    var floatValue: Float {
        return (self as NSString).floatValue
    }
}
