//
//  Money.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 23/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit

class Money: UIViewController,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    var euro : String!
   
    @IBOutlet weak var tb: UITableView!
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if indexPath.row == 0{
        euro = Methods.shared.getEuro()
        var db = Double(euro)
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TableViewCell
        if Double(euro) ?? 0 > 0{
            
            let attributedText = NSMutableAttributedString(string: db!.format(f:".2")+" points", attributes: [ NSAttributedString.Key.foregroundColor: UIColor.green,NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20)])
            
            cell.bitcoin.attributedText = attributedText
            cell.Btn.isHidden = true
            return cell
        }else{
            
            let attributedText = NSMutableAttributedString(string: db!.format(f:".2")+" points", attributes: [ NSAttributedString.Key.foregroundColor: UIColor.red,NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20)])
            
            cell.bitcoin.attributedText = attributedText
            cell.Btn.isHidden = true
            return cell
        }
        }else{
            if !Methods.shared.flag{
           let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1") as! investimento_n
            return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2") as! Time
                return cell
            }
            
        }
        return UITableViewCell()
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
      trigger()
        close()
        
        isModalInPresentation = true
        // Do any additional setup after loading the view.
    }
    
    func trigger(){
        EventManager.shared.listenTo(eventName: "cambia") {
            self.tb.reloadData()
        }
        
    }
    func close(){
        EventManager.shared.listenTo(eventName: "Close") {
            self.dismiss(animated: true) {
                Methods.shared.flag = false
                UserDefaults.standard.set(true, forKey: "flag")
            }
        }
        
    }
    
    @IBAction func close(_ sender: Any) {
        let alertDemo = Popup()
        alertDemo.show(above: self, completion: nil)
    }
    
    func setup(){
        tb.delegate = self
        tb.dataSource = self
        tb.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        tb.register(UINib(nibName: "investimento_n", bundle: nil), forCellReuseIdentifier: "Cell1")
        tb.register(UINib(nibName: "Time", bundle: nil), forCellReuseIdentifier: "Cell2")
        tb.allowsSelection = false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
