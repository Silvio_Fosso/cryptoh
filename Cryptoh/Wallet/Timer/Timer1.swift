//
//  Timer.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 25/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit

class Timer1: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBOutlet weak var time: UILabel!
}
