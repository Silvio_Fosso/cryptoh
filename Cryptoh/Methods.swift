//
//  Methods.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 18/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import Foundation
import UIKit
import FlexibleSteppedProgressBar
class Methods{
    var bia = 0
    var arch = 0
    var descrizione : String!
    var titoloDescrizione : String!
    static let shared = Methods()
     var flag = false
     var flag1 = false
    var orario : String!
    var soldi : String!
    var tb = UITableView()
    var and : Double!
    var finito = false
    var timer = Timer()
    var sender = 0
    var quizFinale = [0,0,0]
    var CambiaReload = false
    var flaginizio = false
    var indicePagina = 0
    var Css = false
    var appoggio = false
    var sezione = 0
    var FINALE = false
    func changeEuro (str : Double,euro : String) -> String{
        var stringa = ""
        
        var appoggio = Double(euro)
        stringa = String(appoggio! + str)
        UserDefaults.standard.setValue(euro, forKey: "eur")
        return stringa
    }
    func getArchBia(){
        if UserDefaults.standard.value(forKey: "bia") != nil && UserDefaults.standard.value(forKey: "arch") != nil{
            
            bia = UserDefaults.standard.value(forKey: "bia") as! Int
            arch = UserDefaults.standard.value(forKey: "arch") as! Int
            
            print(bia)
            print(arch)
        }else{
            UserDefaults.standard.set(0, forKey: "bia")
            UserDefaults.standard.set(0, forKey: "arch")
            
        }
        
    }
    func setarchbia(){
        
        if arch != 2 {
            arch+=1
            UserDefaults.standard.set(arch, forKey: "arch")
            EventManager.shared.trigger(eventName: "vfg")
        }else if bia != 3{
            print(sezione)
            quizFinale[sezione] = 1
            UserDefaults.standard.set(quizFinale, forKey: "quiz")
        }
        EventManager.shared.trigger(eventName: "Vinto")
        EventManager.shared.trigger(eventName: "reload")
        EventManager.shared.trigger(eventName: "aumenta")
        EventManager.shared.trigger(eventName: "azzera")
    }
    func st(){
        if bia < 2{
        self.bia+=1
        self.arch = 0
        UserDefaults.standard.set(self.bia, forKey: "bia")
        UserDefaults.standard.set(self.arch, forKey: "arch")
        EventManager.shared.trigger(eventName: "Vinto")
        EventManager.shared.trigger(eventName: "reload")
        EventManager.shared.trigger(eventName: "aumenta")
        EventManager.shared.trigger(eventName: "azzera")
        }else{
            UserDefaults.standard.set(true, forKey: "FINALE")
            FINALE = true
            EventManager.shared.trigger(eventName: "ultimo")
        }
    }
    
    func getfinale(){
        if UserDefaults.standard.value(forKey: "FINALE") != nil{
        FINALE = UserDefaults.standard.value(forKey: "FINALE") as! Bool
        }
    }
    
    func getEuro() -> String{
        
        return UserDefaults.standard.value(forKey: "eur") as! String
    }
    func getquizfinale (){
        if UserDefaults.standard.value(forKey: "quiz") != nil{
        quizFinale = UserDefaults.standard.value(forKey: "quiz") as! [Int]
            }
        
    }
    
    func tableview (row : Int,tableView :UITableView,euro : String) -> UITableViewCell{
        let dol = Double(euro)
        if(row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TableViewCell
            if Double(euro) ?? 0 > 0{
                
                let attributedText = NSMutableAttributedString(string: dol!.format(f: ".2")+" points", attributes: [ NSAttributedString.Key.foregroundColor: UIColor.green,NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20)])
                
                cell.bitcoin.attributedText = attributedText
            }else{
                let attributedText = NSMutableAttributedString(string: dol!.format(f: ".2")+" points", attributes: [ NSAttributedString.Key.foregroundColor: UIColor.red,NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20)])
                
                cell.bitcoin.attributedText = attributedText
            }
            cell.Btn.isHidden = true
            return cell
        }
        else if (row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1") as! TableViewCell1
             Archivement(bia: bia, numofArch: arch, cell: cell)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2") as! TableViewCell2
            Methods.shared.getquizfinale()
            print(Methods.shared.quizFinale)
            if Methods.shared.quizFinale[0] == 2{
                cell.badge1.image = UIImage(named: "bb")
            }
            if Methods.shared.quizFinale[1] == 2 {
               cell.badge2.image = UIImage(named: "aa")
            }
            
            if Methods.shared.quizFinale[2] == 2 {
                cell.badge3.image = UIImage(named: "cc")
            }
            return cell
            
        }
    }
    
    func Archivement(bia : Int,numofArch : Int,cell : TableViewCell1){
        switch bia {
        case 0:
            cell.progressBar1.currentIndex = numofArch
            setUnderl(progr1: cell.progressBar2, progr2: cell.progressBar3)
            labelDown(lb: cell.adv, lb1: cell.interm)
        break
        case 1:
           cell.progressBar2.currentIndex = numofArch
           setUnderl(progr1: cell.progressBar3, progr2: nil)
           labelDown(lb: cell.adv, lb1: nil)
           cell.progressBar1.currentIndex = 2
        break
            
        case 2:
            cell.progressBar3.currentIndex = numofArch
            cell.progressBar2.currentIndex = 2
            cell.progressBar1.currentIndex = 2
            //labelDown(lb: cell.beginner, lb1: cell.interm)
        break
        default:
            
        break
        }
        
        
        
    }
    
    func setUnderl(progr1 : FlexibleSteppedProgressBar?,progr2 : FlexibleSteppedProgressBar?){
        progr1?.backgroundShapeColor = .systemGray6
        progr1?.centerLayerDarkBackgroundTextColor = .systemGray6
        progr1?.currentSelectedCenterColor = .systemGray6
        progr1?.selectedOuterCircleStrokeColor = .systemGray6
        progr2?.backgroundShapeColor = .systemGray6
        progr2?.centerLayerDarkBackgroundTextColor = .systemGray6
        progr2?.currentSelectedCenterColor = .systemGray6
        progr2?.selectedOuterCircleStrokeColor = .systemGray6
    }
    
    func labelDown(lb : UILabel,lb1 : UILabel?){
        lb.textColor = .systemGray6
        lb1?.textColor = .systemGray6
    }
    
    func ReturnCellController3(row : Int,euro : String,tableView : UITableView) -> UITableViewCell{
        let dol = Double(euro)
        if(row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TableViewCell
            if Double(euro) ?? 0 > 0{
                
                let attributedText = NSMutableAttributedString(string:dol!.format(f:".2")+" points", attributes: [ NSAttributedString.Key.foregroundColor: UIColor.green,NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20)])
                
                cell.bitcoin.attributedText = attributedText
            }else{
                let attributedText = NSMutableAttributedString(string: dol!.format(f:".2")+" points", attributes: [ NSAttributedString.Key.foregroundColor: UIColor.red,NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20)])
                
                cell.bitcoin.attributedText = attributedText
            }
            
            
            return cell
        }else if row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell4") as! TableViewCell4
            //cell.cv.fillSuperView = true
            return cell
        }else if row == 3{
            if UserDefaults.standard.bool(forKey: "attivo") != false{
                let cell = tableView.dequeueReusableCell(withIdentifier: "time") as! Timer1
                EventManager.shared.listenTo(eventName: "tempo") { (tempo) in
                   let tempo1 = tempo as! (Int,Int,Int)
                    cell.time.text = "\(tempo1.0) : \(tempo1.1) : \(tempo1.2)"
                }
                
                return cell
            }
            
            
            if !Methods.shared.flag1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell10") as! Invistimento_p
            return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell5") as! TableViewCell5
                cell.rdn.text = Methods.shared.soldi
                cell.orario.text = Methods.shared.orario
                Methods.shared.flag1 = false
                return cell
            }
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell7") as! TableViewCell7
            
            EventManager.shared.listenTo(eventName: "pinco") { (val) in
                
                
                let str = "Exchange : "+String(describing: val).replacingOccurrences(of: "Optional", with: "").replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "").prefix(8)
                let attributedText = NSMutableAttributedString(string: str+"€", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 24,weight:.bold )])
                
                
                
                cell.ex.attributedText = attributedText
              
                
            }
            
            return cell
        }
        
    }
    
    
    func timer(dati : (Int,Int,Int)){
        var tempo = dati
        
       timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
            if tempo.2 == 0 {
                tempo.2 = 59
                tempo.1 -= 1
            }else{
                tempo.2 -= 1
            }
            if tempo.1 == 0 && tempo.0 == 0 && tempo.2 == 0{
                //EventManager.shared.trigger(eventName: "cambia1")
                Request.shared.getbitcoinNow { (quindi) in
                    var buy = quindi["EUR"]?.buy
                    let soldoni = buy! - (UserDefaults.standard.value(forKey: "valore") as! Double)
                    UserDefaults.standard.set(false, forKey: "attivo")
                    if soldoni >= 0{
                        let eur = Double(Methods.shared.getEuro())
                        let gv = UserDefaults.standard.value(forKey: "soldi1") as! Double
                        UserDefaults.standard.set(String(eur!+soldoni+gv), forKey: "eur")
                        var arrayNot = UserDefaults.standard.value(forKey: "Notifiche") as! [String]
                        print(arrayNot)
                        arrayNot.append(String(soldoni))
                               print(arrayNot)
                        UserDefaults.standard.set(arrayNot, forKey: "Notifiche")
                        DispatchQueue.main.sync {
                            EventManager.shared.trigger(eventName: "reload")
                            EventManager.shared.trigger(eventName: "cambia2")
                        }
                        
                        
                        
                    }else{
                        let eur = Double(Methods.shared.getEuro())
                        print(eur)
                        print(soldoni)
                        UserDefaults.standard.set(String(eur!-soldoni), forKey: "eur")
                        var arrayNot = UserDefaults.standard.value(forKey: "Notifiche") as! Array<String>
                        arrayNot.append(String(soldoni))
                        UserDefaults.standard.set(arrayNot, forKey: "Notifiche")
                        DispatchQueue.main.sync {
                            EventManager.shared.trigger(eventName: "reload")
                            EventManager.shared.trigger(eventName: "cambia2")
                        }
                        
                    }
                     UserDefaults.standard.set(0, forKey: "valore")
                }
                tempo.2+=1
                timer.invalidate()
                 
            }
            
            if tempo.1 == 0 && tempo.2 == 0{
                tempo.0 -= 1
            }
            
            
            
            print("\(tempo.0) : \(tempo.1) : \(tempo.2)")
            EventManager.shared.trigger(eventName: "tempo", information: tempo)
        
        }
    }
    
    
    @IBAction func xxx(_ sender: Any) {
        let url = URL(string: "https://blockchain.info/tobtc?currency=EUR&value=0")!
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            DispatchQueue.main.sync {
                //  self.lb.text = String(data: data, encoding: .utf8)!
                
            }
        }
        
        task.resume()
    }
    
    
}
extension Int {
    func format(f: String) -> String {
        return String(format: "%\(f)d", self)
    }
}

extension Double {
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}
