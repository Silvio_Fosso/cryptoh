//
//  btnImg.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 20/06/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit

class btnImg: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var btn: UIButton!
    var id : Int!
    @IBAction func btn(_ sender: Any) {
        if id == 0{
          EventManager.shared.trigger(eventName: "csv")
        }else if id == 1{
           EventManager.shared.trigger(eventName: "csv1")
        }else{
            EventManager.shared.trigger(eventName: "cs2")
        }
       
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        btn.layer.cornerRadius = 7.0
        // Configure the view for the selected state
    }
    
}
