//
//  QuizTest.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 09/06/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit

class QuizTest: UIViewController,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        4
    }
    
    @IBOutlet weak var btn: UIButton!
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            if !change{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! Quiztest
                cell.flex.currentIndex = self.sbloc
                cell.lb.text = "\(index+1) of 20"
                EventManager.shared.listenTo(eventName: "tempismo", action: {
                    
                    cell.tempo.text = "\(self.minuti) : \(self.secondi)"
                    
                })
                return cell
            }else{
                if mag4{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Finish") as! Finish
                    let count = risposteDate.filter({ $0 == 0}).count
                    cell.lb1.text = "Try Again! your test result is : "
                    btn.isHidden = true
                    cell.lb.text = "\(count) of 20"
                    return cell
                }else{
                    
                    btn.isHidden = true
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Finish") as! Finish
                    let count = risposteDate.filter({ $0 == 0}).count
                    cell.lb.text = "\(count) of 20"
                    return cell
                }
            }
            
            
        }else if indexPath.row == 1{
            if !change {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DM") as! domande
                print(domande[index])
                cell.lb.text = domande[index]
                return cell
            }else
            {
                if mag4{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Soldi") as! Soldini
                    cell.label.text = "No Points!"
                    cell.label.textColor = .red
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Soldi") as! Soldini
                    if Methods.shared.quizFinale[Methods.shared.sezione] == 2 && Methods.shared.Css{
                        cell.label.text = "Complete the other section the get other points!!"
                        cell.label.textColor = .green
                        return cell
                    }
                    
                    if !Methods.shared.Css{
                    cell.label.text = "+10 points"
                    cell.label.textColor = .green
                    let eur = Double(Methods.shared.getEuro())
                    UserDefaults.standard.set(String(eur!+10), forKey: "eur")
                    return cell
                    }else{
                        cell.label.text = "+50 points"
                        cell.label.textColor = .green
                        let eur = Double(Methods.shared.getEuro())
                        UserDefaults.standard.set(String(eur!+50), forKey: "eur")
                        return cell
                    }
                }
            }
        }else if indexPath.row == 2{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Vof") as! Vof
            if !change{
                if index == 19{
                    cell.btn.setTitle("Finish", for: .normal)
                    self.change = true
                    self.secondflag = true
                }
                return cell
            }else{
                if mag4{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Voc") as! Victory
                    cell.voc.image = UIImage(named: "tryAgain")
                    cell.lb.isHidden = true
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Voc") as! Victory
                   
                    if Methods.shared.Css{
                       cell.voc.image = UIImage(named: "bb")
                          
                    }else{
                      cell.voc.image = UIImage(named: "victoryTest")
                      cell.lb.isHidden = true
                    }
                    return cell
                }
              
            
    
                
                
            }
            
            
        }else{
            if !ex{
                let cell = tableView.dequeueReusableCell(withIdentifier: "lb") as! label
                cell.btn.isHidden = true
                if change{
                    ex = true
                }
                if Methods.shared.Css{
                    cell.lb.text = cell.lb.text?.replacingOccurrences(of: "10 points", with: "your Badge")
                }
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "lb") as! label
                cell.btn.isHidden = false
                cell.lb.isHidden = true
                if mag4{
                    cell.btn.setTitle("Try Again !", for: .normal)
                    cell.btn.backgroundColor = .red
                }else{
                    cell.btn.setTitle("Ok!", for: .normal)
                    cell.btn.backgroundColor = .green
                }
                return cell
            }
        }
        return UITableViewCell()
    }
    var secondflag = false
    var domande = [String]()
    var risposte = [Int]()
    var mag4 = false
    var index = 0
    var sbloc = 0
    var minuti = 0
    var secondi = 0
    var risposteDate = [Int]()
    var change = false
    var ex = false
    var flag = false
    @IBOutlet weak var tb: UITableView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        tb.allowsSelection = false
        load()
        tb.delegate = self
        tb.dataSource = self
        // Do any additional setup after loading the view.
        minuti = secondsToHoursMinutesSeconds(seconds: 300).1
        secondi = secondsToHoursMinutesSeconds(seconds: 300).2
        index = 0
        registerxib()
        isModalInPresentation = true
        EventManager.shared.listenTo(eventName:"name") {
            self.index += 1
            print(self.index)
            if (self.index % 5 == 0){
                self.sbloc += 1
            }
           
            if self.risposte[self.index-1] == Methods.shared.sender{
                
                self.risposteDate.append(0)
                print("giusta")
                print(self.risposteDate)
            }else{
                self.risposteDate.append(1)
                print("sbagliata")
            }
            
            
            if self.secondflag{
                let conteggio = self.risposteDate.filter({ $0 == 1}).count
                if conteggio > 4 {
                    self.mag4 = true
                   
                }else{
                     Methods.shared.appoggio = true
                }
            }
            
            self.tb.reloadData()
        }
    }
    
    @IBAction func closebtn(_ sender: Any) {
        let alertDemo = Popup()
        alertDemo.show(above: self, completion: nil)
    }
    
    func close (){
        EventManager.shared.listenTo(eventName: "Close2") {
            self.fg = true
            EventManager.shared.removeListeners(eventNameToRemoveOrNil: "Close2")
            EventManager.shared.removeListeners(eventNameToRemoveOrNil: "name")
            EventManager.shared.removeListeners(eventNameToRemoveOrNil: "tempismo")
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    func load(){
        
        let cs = testQuiz.shared.returnDomandeERisposte()
            domande = cs.0
            risposte = cs.1
            timer()
        
        
    }
 
    func registerxib(){
        
        
        
        tb.register(UINib(nibName: "Quiztest", bundle: nil), forCellReuseIdentifier: "Cell")
        tb.register(UINib(nibName: "domande", bundle: nil), forCellReuseIdentifier: "DM")
        tb.register(UINib(nibName: "Vof", bundle: nil), forCellReuseIdentifier: "Vof")
        
        tb.register(UINib(nibName: "Finish", bundle: nil), forCellReuseIdentifier: "Finish")
        tb.register(UINib(nibName: "Soldini", bundle: nil), forCellReuseIdentifier: "Soldi")
        tb.register(UINib(nibName: "Victoryofal", bundle: nil), forCellReuseIdentifier: "Voc")
        tb.register(UINib(nibName: "label", bundle: nil), forCellReuseIdentifier: "lb")
        close()
    }
    var fg = false
    func timer(){
        let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
            if self.secondi == 0{
                self.secondi = 59
                self.minuti -= 1
            }else{
                self.secondi -= 1
            }
            
            if self.minuti == 0 && self.secondi == 0{
                
                timer.invalidate()
                self.dismiss(animated: true, completion: nil)
            }
            print("entro")
            if self.fg {
                timer.invalidate()
                
                            }
            EventManager.shared.trigger(eventName: "tempismo")
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
}
