//
//  TestQuoz.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 09/06/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import Foundation
class testQuiz{
    static let shared = testQuiz()
    
    
    
    
    func returnDomandeERisposte() ->([String], [Int]){
        var indexwin = [Int]()
        var domande1 = [String]()
        var domande = [String]()
        if !Methods.shared.Css{
        domande = retrievedomande()
        }else{
           domande = returndomandeTestFinaleAcademy()
            
        }
        for i in 0...19{
            let j = Int.random(in: 0...domande.count-1)
            if domande[j].contains("-V"){
                indexwin.append(0)
                domande1.append(domande[j].replacingOccurrences(of: "-V", with: ""))
            }else{
                indexwin.append(1)
                domande1.append(domande[j].replacingOccurrences(of: "-F", with: ""))
            }
            
            domande.remove(at: j)
            
           
            
        }
        return (domande1,indexwin)
    }
    func retrievedomande() -> [String]{

       
        do {
            let path = Bundle.main.path(forResource: "Vof", ofType: "txt")
            let s =  try String(contentsOfFile: path!, encoding: String.Encoding.utf8)
            var ss = s.components(separatedBy: "\r\n")
            ss.remove(at: ss.count-1)
            print(ss)
            return ss
            // process files
        } catch {
           
        }
        return [String]()
    }
    func returndomandeTestFinaleAcademy() -> [String]{
       if Methods.shared.sezione == 0 {
            let domandeBeginner = ["Satoshi Nakamoto is the actual owner of the bitcoin network.-F","Bitcoin is a currency adopted by some countries as an alternative at official one.-F","Bitcoin is the first cryptocurrency in history.-V","A cryptocurrency is any traditional currency stored on an encrypted electronic wallet.-F","Satoshi is a unit of measurement of Bitcoin.-V","Bitcoin was created in 2009 by Satoshi Nakamoto.-V","The maximum value of Bitcoin is fixed to 10.000$.-F"," All Bitcoin transactions can be check and view by everyone in any moment.-V","You can withdraw cash in some ATMs using a Bitcoin account.-V","EThereum is a newer criptocurrency with a higher value of Bitcoin.-F","With Bitcoin you can buy everything if seller accept it.-V","Bitcoin offer a high privacy level about personal information.-V","Investing in Bitcoin is a sure return since not all of them have been generated yet.-F","The first physical purchase was a smartphone for 10.000 BTC.-F","A cryptocurrency use the cryptography to hide identity of owners.-F","Using Bitcoin in Bolivia is illegal.-V","The highest value reached by bitcoin is 20.090$.-V","In the Blockchain eveyone can check the transaction and view the balance of any Bitcoin adress.-V","Depending on local legislation, over a certain amount is necessary to verify identity in order to execute a transaction.-F","If the owner of a Bitcoin wallet lose his private-keys he cannot use the bitcoins in it.-V"]
        return domandeBeginner
       }else if Methods.shared.sezione == 1{
        let domandeIntermediate = ["A miner creates new bitcoins with special machines.-F",
                                   "A wallet allows you to store bitcoin.-V",
                                   "With a wallet you can exchange your bitcoins for fiat money (EUR/USD etc.).-F",
                                   "The creation of bitcoins will stop at 21 million units.-V",
                                   "To buy bitcoins you can use an Exchange.-V",
                                   "Consulting the Blockchain anyone can identify the owner of a BTC address.-F",
                                   "The price of bitcoin is determined according to the law of supply and demand.-V",
                                   "Steps to complete a bitcoin transaction: own a wallet, know the recipient's BTC address, set fees.  –V",
                                   "A P2P network requires each node to share data with the other nodes equally.-V",
                                   "The block reward is given to the miners who have generated the most computing power.-F",
                                   "All nodes that support the blockchain are rewarded with bitcoin.-F",
                                   "The average time to add a Blockchain block is about 1 hour.-F",
                                   "If you lose the private-key of your wallet you will never use your bitcoins.-V",
                                   "A mining farm is a group of miner who work together.-F",
                                   "Thanks to a block explorer you can check all the transactions in Blockchain.-V",
                                   "Bitcoin Core is a Bitcoin-themed forum.-F",
                                   "The blockchain has a maximum attainable size of 10 thousand Terabytes.-F",
                                   "To avoid lost of purchasing power, a limit has been set for the creation of bitcoin.-V",
                                   "You can do mining with any device that has computational power.-V",
                                   "Hash encryption is used in the Bitcoin network to make it safe.-V"]
        return domandeIntermediate
       }else{
        let domandeAdvanced = ["Every 210,000 blocks the 'block reward' is halved.-V",
            "The current 'block reward' value is 12.5 BTC.-F"
            
            ,"The last halving was in May 2020.-V",
             "All bitcoin transactions are covered by a money-back guarantee in case of fraud.-F",
            "The Bitcoin network is totally anonymous and no one can keep track of transactions.-F"
            , "Anyone can view all transactions linked to a BTC address.-V",
            "Bitcoin network is not controlled by any central entity.-V",
            "Young life makes Bitcoin a very volatile commodity.-V",
            "The mempool is where all cancelled transactions are recorded.-F",
            "When the payer does not have enough bitcoin to complete the transaction it is reported to the beneficiary as 'unconfirmed'.-F",
            "Miners confirm a transaction only after add it in the first available block.-V",
            "The 'mempool' size increases with the number of 'unconfirmed' transactions.-V",
            "In order to have a realistic chance of earning something with mining you need to enter a mining pool.-V",
            "A 'mining pool' is a building where mining machines are rent.-F", "New bitcoins are given to miners who add a 'block' to the Blockchain.-V",
            "A 'block' of the Blockchain is an event when network shut down.-F"
            ,"A 'block' contains a number of transactions confirmed by miners.-V", "The power of a mining machine is indicated with the hash rate .-V","If you want complete a transaction faster you will have to pay higher fees.-V",
            "The higher the Network difficulty the more time it will take to confirm a transaction. -F"
        ]
        return domandeAdvanced
        }
        
    }
    
    
}
