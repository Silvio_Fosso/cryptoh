//
//  Quiztest.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 09/06/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit
import FlexibleSteppedProgressBar
class Quiztest: UITableViewCell,FlexibleSteppedProgressBarDelegate {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var flex: FlexibleSteppedProgressBar!
    
    @IBOutlet weak var lb: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        flex.numberOfPoints = 4
        flex.lineHeight = 9
        flex.radius = 15
       flex.progressRadius = 25
        
       flex.currentSelectedCenterColor = .orange
       flex.progressLineHeight = 3
        flex.delegate = self
        // Configure the view for the selected state
    }
    @IBOutlet weak var tempo: UILabel!
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     textAtIndex index: Int, position: FlexibleSteppedProgressBarTextLocation) -> String {
        return ""
    }
}
