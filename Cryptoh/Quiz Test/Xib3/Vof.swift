//
//  Vof.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 09/06/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit

class Vof: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var btn: UIButton!
    
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        btn.layer.cornerRadius = 7.0
        btn.isEnabled = false
        Verobtn.layer.cornerRadius = 7.0
        falsoBtn.layer.cornerRadius = 7.0
        // Configure the view for the selected state
    }
    
    @IBOutlet weak var falsoBtn: UIButton!
    @IBOutlet weak var Verobtn: UIButton!
    @IBAction func settab(_ sender: UIButton) {
        Methods.shared.sender = sender.tag
        btn.isEnabled = true
        if sender.tag == 0
        {
            falsoBtn.backgroundColor = .clear
            Verobtn.backgroundColor = UIColor.init(hexString: "#EA931E")
        }else{
            falsoBtn.backgroundColor = UIColor.init(hexString: "#EA931E")
            Verobtn.backgroundColor = .clear
        }
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
    }
    @IBAction func premuto(_ sender: Any) {
        Verobtn.backgroundColor = .clear
        falsoBtn.backgroundColor = .clear
        EventManager.shared.trigger(eventName: "name")
        
    }
}
extension UIColor {
    convenience init?(hexString: String) {
        var chars = Array(hexString.hasPrefix("#") ? hexString.dropFirst() : hexString[...])
        switch chars.count {
        case 3: chars = chars.flatMap { [$0, $0] }; fallthrough
        case 6: chars = ["F","F"] + chars
        case 8: break
        default: return nil
        }
        self.init(red: .init(strtoul(String(chars[2...3]), nil, 16)) / 255,
                  green: .init(strtoul(String(chars[4...5]), nil, 16)) / 255,
                  blue: .init(strtoul(String(chars[6...7]), nil, 16)) / 255,
                  alpha: .init(strtoul(String(chars[0...1]), nil, 16)) / 255)
    }
}
