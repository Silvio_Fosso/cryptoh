//
//  Bottone.swift
//  Cryptoh
//
//  Created by Christian on 28/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit

class Bottone: UITableViewCell {

    @IBOutlet weak var Btnok: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func click(_ sender: Any) {
        EventManager.shared.trigger(eventName: "click")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
