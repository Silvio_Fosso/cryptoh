//
//  Notifiche.swift
//  Cryptoh
//
//  Created by Christian on 28/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit

class Notifiche: UIViewController , UITableViewDelegate,UITableViewDataSource{
    var notifiche =  [""]
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          
        return notifiche.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Notifica") as! Notifications
        if(notifiche[indexPath.row] == " ")
        {
             cell.lb.text = "NOTIFICATIONS"
            
        }
            else
            {
                let dol = Double(notifiche[notifiche.count-indexPath.row])
                if notifiche[notifiche.count-indexPath.row].contains("-")
                    {
                        cell.lb.text = "You have lose " + (dol?.format(f: ".2"))!+"€"
                        cell.lb.textColor = UIColor.red
                    }
                else
                    {
                        cell.lb.text = "You have gained " +  (dol?.format(f: ".2"))!+"€"
                        cell.lb.textColor = UIColor.green
                    }
            }
         return cell
        
        
        



    }
    
    func registerxib(){
        table.delegate = self
        table.dataSource = self
        table.register(UINib(nibName: "Notifications", bundle: nil), forCellReuseIdentifier: "Notifica")
        table.register(UINib(nibName: "Bottone", bundle: nil), forCellReuseIdentifier: "btn")
    }

    @IBOutlet var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        notifiche =  UserDefaults.standard.value(forKey: "Notifiche") as! [String]
        print(notifiche)
        registerxib()
        table.reloadData()
        clic()
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    func clic()
    {
        EventManager.shared.listenTo(eventName: "click") {
            self.dismiss(animated: true) {
                
            }
        }
    }

}
