//
//  ViewController3.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 18/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit
import HITChartSwift
class ViewController3: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var c  = 4
    static let shared = ViewController3()
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return c
    }
    var euro = "0.00"
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let euro1 = UserDefaults.standard.value(forKey: "eur") as! String
        return Methods.shared.ReturnCellController3(row: indexPath.row, euro: euro1, tableView: tableView)
        
    }
    
   
   
    @IBOutlet weak var tb: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
       tb.allowsSelection = false
        euro = Methods.shared.getEuro()
        registerxib()
        // Do any additional setup after loading the view.
        st()
        Ts()
        Not()
        Methods.shared.tb = tb
        Ts1()
    }
    func registerxib(){
        
        tb.delegate = self
        tb.dataSource = self
        UserDefaults.standard.set(true, forKey: "flag")
        tb.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        tb.register(UINib(nibName: "TableViewCell4", bundle: nil), forCellReuseIdentifier: "Cell4")
        tb.register(UINib(nibName: "Invistimento_p", bundle: nil), forCellReuseIdentifier: "Cell10")
        tb.register(UINib(nibName: "TableViewCell7", bundle: nil), forCellReuseIdentifier: "Cell7")
        self.tb.register(UINib(nibName: "TableViewCell6", bundle: nil), forCellReuseIdentifier: "Cell5")
        self.tb.register(UINib(nibName: "Timer1", bundle: nil), forCellReuseIdentifier: "time")
        
       
        
        
        
    }
    
    func Not(){
        EventManager.shared.listenTo(eventName: "Notifiche") {
              self.performSegue(withIdentifier: "Notifiche", sender: nil)
        }
    }
  
    func st (){
        EventManager.shared.listenTo(eventName: "Money") {
            self.performSegue(withIdentifier: "Money1", sender: nil)
        }
    }
    func Ts(){
        EventManager.shared.listenTo(eventName: "cambia1") {
            self.tb.beginUpdates()
            
            self.tb.reloadRows(at: [IndexPath(row: 1, section: 0),IndexPath(row: 3, section: 0),IndexPath(row: 2, section: 0)], with: .automatic)
            self.tb.endUpdates()
            
            
            //Methods.shared.flag1 = false
        }
    }
    func Ts1(){
        EventManager.shared.listenTo(eventName: "cambia2") {
            self.tb.beginUpdates()
            
            self.tb.reloadRows(at: [IndexPath(row: 3, section: 0),IndexPath(row: 0, section: 0)], with: .automatic)
            self.tb.endUpdates()
            
            
            //Methods.shared.flag1 = false
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
   
    
    
    
}

extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}
