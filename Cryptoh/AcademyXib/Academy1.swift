//
//  Academy1.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 20/06/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit
import UICircularProgressRing
class Academy1: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var lb1: UILabel!
    @IBOutlet weak var lb: UILabel!
    
    @IBOutlet weak var learn: UIButton!
    @IBOutlet weak var circle: UICircularProgressRing!
    @IBOutlet weak var view1: UIView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
learn.layer.cornerRadius = 7.0
        // Configure the view for the selected state
    }
    @IBAction func btn(_ sender: Any) {
        EventManager.shared.trigger(eventName: "academytrigger")
    }
    
}
