//
//  File.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 20/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import Foundation
import UIKit
class DR{
    var index: Int = 0
    static let shared = DR()
    var domandeBegginer0 = [String]()
    var risposteBeginner0 = [String]()
     var win = [Int]()
    var items = [(String, String,String,String,String)]()
    var Descrizione = [String]()
    func Inizialize(bia : Int, arch : Int){
        index = 0
        items.removeAll()
        win.removeAll()
        domandeBegginer0 = RispEDom.shared.returnDomanda(bia: bia, arch: arch)
        risposteBeginner0 = RispEDom.shared.returnRisposte(bia: bia, arch: arch)
        Descrizione = RispEDom.shared.returnDescrizione(bia: bia, arch: arch)
        getwin()
        riempiItem()
    }
    
    func getwin(){
        var k = 0
        for i in 0...risposteBeginner0.count-1{
            if(i%4 == 0){
                k = 0
            }
            if(risposteBeginner0[i].contains("-1"))
            {
                win.append(k)
                risposteBeginner0[i] = risposteBeginner0[i].replacingOccurrences(of: "-1", with: "")
                
            }
            k+=1
        }
    }
    func riempiItem(){
        var k = 0
        for i in 0...domandeBegginer0.count-1{
            items.append((domandeBegginer0[i], risposteBeginner0[k], risposteBeginner0[k+1], risposteBeginner0[k+2], risposteBeginner0[k+3]))
            k+=4
        }
    }
    func startgame(domanda : UILabel) -> [String]{
        print(index)
        domanda.text = items[index].0
        return [items[index].1,items[index].2,items[index].3,items[index].4]
    }
    func getWin() -> [Int]{
        
        return win
    }
    func setIndex(){
        index+=1
    }
    func getindex ()->Int{
        return index
    }
    func getItems() -> Int{
        return items.count
    }
    
    func getDescrizione() -> [String]{
        return Descrizione
    }
    
    
}
