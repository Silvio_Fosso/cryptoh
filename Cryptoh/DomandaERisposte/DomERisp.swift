//
//  DomERisp.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 20/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import Foundation
class RispEDom {
    static let shared = RispEDom()
    let domandeBegginer0 = ["What is cryptocurrency?","What's Bitcoin?","What was the first cryptocurrency in history?","When was Bitcoin created?"]
    var risposteBeginner0 = ["An electronic online payment system.","An alternative currency to the traditional ones-1","Coin on digital wallets.","A secure payment system using cryptography.","A virtual register of all credit cards","A secure and stable financial instrument.","A cryptocurrency and his network-1","An alternative official currency.","Bitcoin-1","Ripple.","Bitcoin Cash.","Ethereum.","2012","2009-1","Nobody knows.","It's always existed."]
    
    var descrizioneBegginer0 = ["A cryptocurrency is a coin represented only virtually based on cryptography and which does not refer to any central authority.","For Bitcoin we mean the famous cryptocurrency, the whole network and the technology used to make it work.","Bitcoin was the first cryptocurrency developed. In the wake of Bitcoin's success, many other cryptocurrencies were created over time.","Bitcoin was created in 2009. Its initial value was 1309 BTC for 1 USD."]
    //-------------------------------------------
    let domandeBegginer1 = ["What was the highest value reached by the bitcoin ( until May 2020) ?","What can I buy with bitcoins?","Can I lose the bitcoins I own?","Is bitcoin legal?"]
    var risposteBeginner1 = ["12.120 $","5.000 $","20,090 $-1","9.500 $","Any digital goods as long as they are under 1000€.","Any physical and digital goods for sale online only.","Any physical and digital goods for sale online and physically.-1","Only more coins, both crypto and fiat money.","No, it is impossible to lose them.","Yes if I lose my access keys.-1","Yes if I cannot prove that I am the owner of the wallet.","Yes if someone finds out my password.","Yes","Only in USA","No","Depends on Government-1"]
    var descrizioneBeginner1 = ["On 17th December 2017 the bitcoin registered its historical record of 20,089 USD. After 30 days, it had already dropped to $10,000, halving its value.","Bitcoins are increasingly accepted in the physical world as well. There are some ATMs that allow you to make a change in BTC with cash money and even in some countries it is possible to pay for some public administration services.","Create a backup file containing the private keys of our wallet is the first thing to do if I want to be sure you can always access your bitcoins.","In some countries like Bolivia and Ecuador bitcoin has been banned. In most of the world it is legal but not regulated by any law. Few countries recognize it as a real legal currency."]
    
    
    //---------------------------------------------
    let domandeBegginer2 = ["Is it safe to invest in bitcoin?","What is the purpose of cryptography in the Bitcoin network?","Which of the following is NOT a bitcoin(BTC) unit?","What was the first physics to be bought with bitcoins?"]
    var risposteBeginner2 = ["It's a safe profit.","It's a sure loss.","It depends on the amount invested.", "It's risky.-1","To keep commissions low.","To give you the ability to hide your identity.","To facilitate transactions.","To make the Bitcoin network secure.-1","μBTC","mBTC","eBTC-1", "sat","Pizza.-1","An electronic device.","A car.","A ticket for public transport."]
    var descrizioneBeginner2 = ["Like most financial instruments, bitcoins are a high risk investment. Their volatility has allowed for large gains but also large losses.","Encryption is a discipline used for data obscuring. In the Bitcoin network it is used to make transactions secure.","1 μBTC(bit)= 0.000001 BTC; 1 mBTC= 0.001 BTC; 1 sat (satoshi) = 0.00000001 BTC","On 22 May 2010 a programmer, Laszlo Hanyecz, paid BTC 10.000 for two pizza. Ten years later that sum is valued at 91 million Dollars."]
    
//    -----------------------------------------------
   let domandeIntermedio0 = ["What is a bitcoin wallet?","What is the maximum limit of Bitcoin that will be in circulation in the future?","What's the blockchain?","What is a P2P network?"]
    let risposteIntermedio0 = ["A service that allows you to buy and sell bitcoins.","A service to manage bitcoins owned.-1","A platform to explore the transactions noted in the blockchain.","None of the previous.","There is no limit","Approximately 1 billion","21 Million-1","94 Million","A public network where everyone can use the addresses provided by the community.","An encrypted record that stores all wallets private-keys.","A series of Bitcoin transactions to the same address recorded in a data 'block'.","A public record of Bitcoin transactions organised in chronological order.-1","A network where all members transmit data as equals.-1","A private sharing cloud.","A IT forum.","An environment reserved for Bitcoin nodes."]
    let descrizioneIntermedio0 = ["A bitcoin wallet allows you to store, send  and receive bitcoins in a secure way thanks to encrypted keys. A wallet can be implemented for desktop or mobile devices, or on hardware supports.","The maximum number of Bitcoins in circulation will be in 2140 with an amount of 21 million. To today there are already about 18.5 million bitcoin in circulation.","The blockchain is the ledger of the Bitcoin Network. By consulting the blockchain you can review the history of transactions made by anyone at any time. ","A peer-to-peer network is designed in order that all nodes act as both servers and clients, sharing data as peer without being hierarchized."]
//------------------------------------------
    let domandeIntermedio1 = ["What is a Bitcoin Exchange?","Why does Bitcoin production have a fixed maximum limit?","What's hash?","How is quoted the price of bitcoin?"]
    let risposteIntermedio1 = ["It is a service that allows you to sell and buy Bitcoin.-1 ","It is a platform that connects the members of the Bitcoin network.","Any service that allows you to view the Bitcoin exchange rate in real time.","The official forum where the community can discuss and share opinions about Bitcoin.","Because it is illegal to produce too many of them","Because at some point updating the block chain will require too much computing power.","To avoid too high inflation.-1","None of the above.","A unit of measurement of Bitcoin.","Output of a mathematical algorithm.-1","Mining machine response time."," None of the above.","It is imposed by the Bitcoin core network.","It depends on the number of 'miners' working at that moment.","It is based on the law of supply and demand.-1","It increases progressively until a fixed maximum limit is reached."]
    let descrizioneIntermedio1 = ["A Bitcoin Exchange allows, by some fees, to sell and buy cryyptocurrency using fiat money. One of the most popular Exchange is Coinbase.","The bitcoin, unlike the fiat money, has been programmed with a maximum production limit, inspired by the gold system. This is to prevent the currency from losing its buying power over time.","It is the output of a mathematical function that processes any number of characters. It will be unique, fixed length and irreversible.","As it happens in the stock market and for fiat money the price of the bitcoin changes according to the law of supply and demand. More people buy bitcoins, higher is the price."]
//------------------------------------------
    let domandeIntermedio2 = ["What is 'block explorer'?","What is NOT necessary to conclude a bitcoin transaction?","What is the average time to add a new 'block' to the Blockchain?","What's a 'block reward'? "]
    let risposteIntermedio2 = ["The action of checking that a transaction is valid.","A browser designed exclusively for bitcoin management.","A member of the network that controls the integrity of the Blockchain 'blocks'."," A tool that allows the navigation inside the Blockchain.-1","Own a wallet.","Know the destination BTC address.","Pay some fees.","Be logged into Bitcoin Core.-1","2 hours","5 days","3 minutes","10 minutes-1","A bitcoin reward distributed equally at all active nodes of the Bitcoin network.","New bitcoins introduced in the network.-1","A prize recognised to the most honest members of the Network.","All the fees of a 'block' paid to the miners."]
    let descrizioneIntermedio2 = ["A block explorer or Blockchain browser allows you to navigate within the Blockchain allowing you to check every transaction, the balance of a BTC address and check network statistics.","You do not need Bitcoin Core, an open-source node management software, to proceed with a transaction.","On average, a block takes about 10 minutes to be solved and added to the Blockchain. ","The block reward is given to the miner who solves a transactions 'block' of the Blockchain. It consists of a fixed number of new bitcoins generated by the network."]

//------------------------------------------
   let domandeAdvanced0 =  ["Which of the following statements about Bitcoin transactions is true?","How are bitcoins produced?","What's a 'block' of the blockchain?","What are Blockchain nodes? "]
    
  let risposteAdvanced0 = ["Bitcoin transactions are public and traceable.-1","The Bitcoin network is totally anonymous and no one can keep track of transactions.","A transaction can be cancelled at any time by the payer unless the amount has already been transferred by the beneficiary to another BTC account.","All transactions for goods and services are covered by a money-back guarantee in case of fraud.","They are distributed randomly to people who work to secure the network.-1 ","They are created by special machines and software.","They are created when the transactions increase significantly.","They are created by central authorities known as Exchange, a sort of bitcoin banks.","A cluster of all rejected transactions.","A sudden system shutdown.","A group of confirmed transactions.-1","The source code of the blockchain.","All devices connected to the Blockchain.","Transaction control points.","Servers that host the Blockchain network.","All the above are correct.-1"]
    
  let descrizioneAdvanced0 =  ["All bitcoin transactions are tracked and can be viewed by anyone on the blockchain. You can also view all transactions linked to a BTC address.","New bitcoins are automatically created by the network each time a new 'block' is added and the blockchain updated. These are distributed to the 'mining pool' that added the 'block'.","The blockchain consists of blocks that are consecutively linked together.  Each block contains a number of transactions confirmed by miners.","A node is any device that connects to the P2P network, being client and server at the same time. There are various types of nodes that actively collaborate on Blockchain security."]
//------------------------------------------
   let domandeAdvanced1 =  ["What is a Bitcoin halving?","Which of these sets of terms best describes the Bitcoin network?","What's a mining pool?","How do the fees for sending Bitcoin change?"]
    
      let risposteAdvanced1 = ["The increase of the fees for each transaction.","When the value of bitcoin drops until halve its value.","An event that halves the reward of the 'miners' for each block.-1","The event that indicates that half of the total bitcoins have been created.","ecentralized, Volatile, Open-source.-1 ","Centralized, Stable, Open-source.","Decentralized, Anonymous, Open-source.","Decentralized, Immutable, Proprietary-system.","A building where there are a large number of mining machines.","A group of people dedicated to mining.-1","An inaccesible virtual place where all the bitcoins not yet extracted are stored."," A platform that allows mining through the use of remote servers.","They depend on the amount of BTC.","They depend on the wallet used.","They are fixed.","They depend on network congestion.-1"]
    
      let descrizioneAdvanced1 = ["Every 210.000 blocks (approximately every 4 years), the 'miners' reward which is a sum of newly created bitcoins, is halved. In this way the production of new bitcoins slows down progressively.","Contrarily to fiat money, the Bitcoin network is free from the control of a central authority and anyone can collaborate to make it safe. In its history the bitcoin has often had rapid price changes making it a very volatile commodity.","A mining pool is a network where people providing the computing power of their machines join together to have higher chance of receiving a mining reward.","Commission fees are the second largest source of profits for miners, so when a lot of transactions are made they will give proirity to transactions with higher fees."]
    //------------------------------------------
  let domandeAdvanced2 =   ["How much is the block reward currently?","What's the size of the 'mempool'? ","Which of these sets of terms best describes Blockchain technology?","What's the hash rate? "]
    
     let risposteAdvanced2 =  ["12.5 BTC ","6.25 BTC-1 ","Depends on transaction fees","0.25 BTC","About 34 GB","About 34 MB","Unlimited size","Depends on the number of transactions.-1 ","Changeable, Public, Dentralized, Safe.","Unchangeable, Public, Decentralized, Secure.-1","Changeable, Reserved, Centralized, Secure.","Unchangeable, Reserved, Decentralised, Secure.","The computing power used for the extraction of new 'blocks'.-1","It indicates the volume of transactions carried out over a period of time.","The amount of bitcoins that can be sent in a single transaction.","The speed a transaction is received."]
    
     let descrizioneAdvanced2 =  ["Currently the block reward is 6.25 BTC. The block reward is halved every 4 years or so. The last halving was in May 2020.","The mempool size increases with the number of unconfirmed transactions. The average is from 3 MB to 10 MB, but sometimes up to 120 MB.","The Blockchain was born with the purpose of being transparent and self-sustainable, so it can be free from the control of a central authority. It is impossible to modify, preventing any manipulations. ","The hash rate, (H/s) is the unit to measure the speed of mining machines. A hight rate means more powerful the machine and a better chance of mining a 'block'."]
    
    func returnDomanda(bia : Int,arch : Int) -> [String]{
        switch bia {
        case 0:
            if arch == 0{
                return domandeBegginer0}
            else if arch == 1 {return domandeBegginer1}
            else if arch == 2 {return domandeBegginer2}
        case 1:
            if arch == 0{
                return domandeIntermedio0}
            else if arch == 1 {return domandeIntermedio1}
            else if arch == 2 {return domandeIntermedio2}
            break
            
        case 2 :
            if arch == 0{
                return domandeAdvanced0}
            else if arch == 1 {return domandeAdvanced1}
            else if arch == 2 {return domandeAdvanced2}
        break
        default:
            break
        }
        return [String]()
        
    }
    
   
    
    func returnRisposte(bia : Int,arch : Int) -> [String]{
        switch bia {
        case 0:
            if arch == 0{
                return risposteBeginner0}
            else if arch == 1 {return risposteBeginner1}
            else if arch == 2 {return risposteBeginner2}
            
        case 1:
            if arch == 0{
                return risposteIntermedio0}
            else if arch == 1 {return risposteIntermedio1}
            else if arch == 2 {return risposteIntermedio2}
            
            break
            
        case 2 :
            if arch == 0{
                return risposteAdvanced0}
            else if arch == 1 {return risposteAdvanced1}
            else if arch == 2 {return risposteAdvanced2}
            break
        default:
            break
        }
        return [String]()
    }
    
    func returnDescrizione(bia : Int,arch : Int) -> [String]{
        switch bia {
        case 0:
            if arch == 0{
                return descrizioneBegginer0}
            else if arch == 1 {return descrizioneBeginner1}
            else if arch == 2 {return descrizioneBeginner2}
        case 1:
            if arch == 0{
                return descrizioneIntermedio0}
            else if arch == 1 {return descrizioneIntermedio1}
            else if arch == 2 {return descrizioneIntermedio2}
            break
            
        case 2 :
            if arch == 0{
                return descrizioneAdvanced0}
            else if arch == 1 {return descrizioneAdvanced1}
            else if arch == 2 {return descrizioneAdvanced2}
            break
        default:
            break
        }
        return [String]()
    }
    
}
