//
//  QuizFinaleText.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 15/06/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit

class QuizFinaleText: UIViewController {
    var lv : String!
    @IBOutlet weak var lb: UILabel!
    @IBAction func btn1(_ sender: Any) {
        let alertDemo = Popup()
        alertDemo.show(above: self, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        convert(livello: Methods.shared.bia)
        btn.layer.cornerRadius = 7.0
        lb.text = "Let’s start with the quiz!\nAnswering these questions\nyou can get the Level \(lv ?? "")!"
        chiudi()
    }
    
    @IBOutlet weak var btn: UIButton!
    func convert(livello : Int)
    {
        switch livello {
        case 0:
            lv = "Beginner"
        case 1:
            lv = "Intermediate"
        case 2:
            lv = "Advanced"
        default:
            break
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    @IBAction func btn(_ sender: Any) {
        Methods.shared.Css = true
        performSegue(withIdentifier: "ml", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      
    }
    func chiudi(){
        EventManager.shared.listenTo(eventName: "ciao") {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
