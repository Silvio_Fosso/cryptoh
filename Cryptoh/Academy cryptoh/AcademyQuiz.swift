//
//  AcademyQuiz.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 20/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit
import FlexibleSteppedProgressBar
class AcademyQuiz: UIViewController,UITableViewDelegate,UITableViewDataSource,FlexibleSteppedProgressBarDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    @IBOutlet weak var flex: FlexibleSteppedProgressBar!
    @IBOutlet weak var submit: UIButton!
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! Quiz
       
        cell.lb.text = risposte[indexPath.row]
        submit.isEnabled = false
        return cell
        
    }
    @IBOutlet weak var statogame: UILabel!
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! Quiz
        selected = indexPath.row
        submit.isEnabled = true
      
    }
    var selected = 0
    var index = 0
    var risposte = [String]()
    @IBOutlet weak var domanda: UILabel!
    @IBOutlet weak var livello: UILabel!
    var win = [Int]()
    var elem = 0
    var spiegazioni = [String]()
    var bia : Int!
    var arch : Int!
    var livello1 : String!
    @IBOutlet weak var tb: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        xib()
        livello.text = livello1
        // Do any additional setup after loading the view.
        isModalInPresentation = true
        DR.shared.Inizialize(bia: bia,arch: arch)
       risposte =  DR.shared.startgame(domanda:domanda)
        win = DR.shared.getWin()
        elem = DR.shared.getItems()
        setlvl()
        spiegazioni = DR.shared.getDescrizione()
        Close()
        tb.separatorStyle = .none
        flex.numberOfPoints = 5
        flex.lineHeight = 9
        flex.radius = 15
        flex.currentIndex = index
        flex.progressRadius = 25
        flex.currentSelectedCenterColor = .orange
        flex.progressLineHeight = 3
        flex.delegate = self
       
    }
    func setlvl(){
        let lvl = checkStatus(bia: UserDefaults.standard.value(forKey: "bia") as! Int)
        let adv = String(describing:UserDefaults.standard.value(forKey: "arch")).replacingOccurrences(of: "Optional", with: "").replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "")
        submit.layer.cornerRadius = 7.0
        let index = DR.shared.getindex()
        statogame.text = String(index+1)+" of "+String(win.count)
    }
    
    func xib(){
        tb.isScrollEnabled = false
        tb.delegate = self
        tb.dataSource = self
         tb.register(UINib(nibName: "Quiz", bundle: nil), forCellReuseIdentifier: "Cell")
    }
    
    @IBAction func close(_ sender: Any) {
        let alertDemo = Popup()
        alertDemo.show(above: self, completion: nil)
    }
    func Close(){
        EventManager.shared.listenTo(eventName: "Close1") {
            self.dismiss(animated: false) {
               
            }
        }
    }
    
    @IBOutlet weak var spiegazione: UILabel!
    func checkStatus(bia : Int) -> String{
        if bia == 0{return "Beginner"}
        else if bia == 1{ return "Intermedia"}
        else{return "Advanced"}
           
    }
    var presegiuste = 0
    var contatore = 0
    @IBAction func btn(_ sender: UIButton) {
        var index = DR.shared.getindex()
        if contatore != 0{
        contatore = 0
        DR.shared.setIndex()
        index = DR.shared.getindex()
            submit.setTitle("Submit", for: .normal)
            submit.backgroundColor = UIColor.init(hexString: "#ff9808")
            spiegazione.text = ""
        if(index < elem){
            Methods.shared.descrizione = spiegazioni[index-1]
            Methods.shared.titoloDescrizione = risposte[win[index-1]]
            let controller = storyboard!.instantiateViewController(withIdentifier: "Descr")
            
            addChild(controller)
            controller.view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
            controller.view.tag = 1
                view.addSubview(controller.view)
            controller.didMove(toParent: self)
            
            removefrom(controller: controller)
            risposte = DR.shared.startgame(domanda: domanda)
            tb.reloadData()
             statogame.text = String(index+1)+" of "+String(win.count)
        }else{
            if presegiuste >= 3 && arch == Methods.shared.arch{
                Methods.shared.setarchbia()
            }
            Methods.shared.descrizione = spiegazioni[index-1]
            Methods.shared.titoloDescrizione = risposte[win[index-1]]
            Methods.shared.finito = true
            let controller = storyboard!.instantiateViewController(withIdentifier: "Descr")
            
            addChild(controller)
            controller.view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
            controller.view.tag = 1
            view.addSubview(controller.view)
            controller.didMove(toParent: self)
            
           
           // self.dismiss(animated: true, completion: nil)
        }
        }else{
            if(selected == win[index]){
                print("win")
                presegiuste += 1
                submit.backgroundColor = UIColor.init(hexString: "#07ab3e")
                submit.setTitle("Discover More", for: .normal)
                spiegazione.text = "Great! Congrats"
            }else{
                print("lose")
                submit.backgroundColor = .red
                submit.setTitle("Learn together", for: .normal)
                spiegazione.text = "Don't worry , keep going !"
            }
            contatore += 1
        }
    }
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     textAtIndex index: Int, position: FlexibleSteppedProgressBarTextLocation) -> String {
        return ""
    }
    func removefrom(controller : UIViewController){
        EventManager.shared.listenTo(eventName: "Descr") {
            if let viewWithTag = self.view.viewWithTag(1) {
                viewWithTag.removeFromSuperview()
            }else{
               
            }
        }
    
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

