//
//  SecondViewController.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 18/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    static let shared = SecondViewController()
    var euro = ""
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    var cont = 0
    
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let euro1 = UserDefaults.standard.value(forKey: "eur") as! String
            return  Methods.shared.tableview(row: indexPath.row, tableView: tableView,euro: euro1)
            
        }
    
    

    @IBOutlet weak var talbeview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        talbeview.allowsSelection = false
       Methods.shared.getfinale()
        // Do any additional setup after loading the view.
        talbeview.frame.size.width = UIScreen.main.bounds.width
        talbeview.frame.size.height = UIScreen.main.bounds.height
        ReloadEvent()
       
        if let btc =  UserDefaults.standard.value(forKey: "eur"){
            euro = btc as! String

        }else{
            
            UserDefaults.standard.setValue("0.00", forKey: "eur")
            euro = "0.00"
            let arrayNot = [" "]
             UserDefaults.standard.set(arrayNot, forKey: "Notifiche")
 
        }
        registerxib()
        
    }
    func registerxib(){
        
        talbeview.delegate = self
       talbeview.dataSource = self
        UserDefaults.standard.set(false, forKey: "flag")
        talbeview.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        talbeview.register(UINib(nibName: "TableViewCell1", bundle: nil), forCellReuseIdentifier: "Cell1")
        talbeview.register(UINib(nibName: "TableViewCell2", bundle: nil), forCellReuseIdentifier: "Cell2")
       
        
        
    }
    
    
    
    func ReloadEvent(){
        EventManager.shared.listenTo(eventName: "reload") {
            self.talbeview.reloadData()
        }
        
    }
    func ReloadEvent1(){
        EventManager.shared.listenTo(eventName: "reload1") {
            DispatchQueue.main.sync {
                self.talbeview.reloadData()
            }
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Methods.shared.getArchBia()
         UserDefaults.standard.set(false, forKey: "flag")
    }
    
    
    
        
        
    
    


}

