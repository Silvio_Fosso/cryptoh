//
//  TableViewCell5.swift
//  
//
//  Created by Silvio Fosso on 19/05/2020.
//

import UIKit
import UserNotifications
class TableViewCell5: UITableViewCell {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    @IBOutlet weak var orario: UITextField!
   
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    @IBOutlet weak var rdn: UITextField!
    
    @IBOutlet weak var btn: UIButton!
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
      
        // Configure the view for the selected state
    }
    //30 minuti = 1800 sec
    //1 ora = 3600sec
    //8 ore = 28800sec
    //1 giorno  = 86400sec
    //3 giorni = 259200sec
    @IBAction func pressed(_ sender: Any) {
       
        let notificationCenter = UNUserNotificationCenter.current()
        
        
        
        let notificationBody = UNMutableNotificationContent()
        notificationBody.title = "Cryptoh";
        notificationBody.body = "Hey check if you granted some coint"
        //Per l'immagine, se ti spaccano la minchia riferisci che l'immagine è l'icona dell'app
        //Qui crei il trigger, la prova che faccio è attivarle ogni 20 secondi, presumibilmente con un event listner si può fare altro. Pensandoci, potresti usare anche questo come calendario settando il timeInterval in base al lasso di tempo scelto.
        
        
        let seconds = returnSecond(dg: Methods.shared.orario)
        let date = Date().addingTimeInterval(seconds)
        
        let dateComponents = Calendar.current.dateComponents([.year, .month, .minute, .second], from: date)
        
        //Crei il trigger, ma copialo da qui che va bene LUL
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        
        
        //Assegni un identificativo a ste notifiche che mandi, che corrisponde all'ID del device.
        
        let uuidString = UUID().uuidString
        
        let request = UNNotificationRequest(identifier: uuidString, content: notificationBody, trigger: trigger)
        
        notificationCenter.add(request) { (error) in
            if let error = error {
                print("There was an error")
            } else {
                let now = Date().timeIntervalSince1970
                
               
                    
                UserDefaults.standard.set(date.timeIntervalSince1970, forKey: "orario")
                UserDefaults.standard.set(true, forKey: "attivo")
                DispatchQueue.main.sync {
                    var sold = Double(Methods.shared.getEuro())
                    var dim =  Double(Methods.shared.soldi.replacingOccurrences(of: ",", with: "."))
                    UserDefaults.standard.set(dim, forKey: "soldi1")
                    UserDefaults.standard.set(String(sold!-dim!), forKey: "eur")
                    FirstViewController().checkTrade()
                    EventManager.shared.trigger(eventName: "reload")
                    EventManager.shared.trigger(eventName: "cambia2")
                    UserDefaults.standard.set(Methods.shared.and, forKey: "valore")
                   
                }
              
                    
                    
                
            }
        }

        
    }
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    func returnSecond(dg : String)->Double{
        switch dg {
        case "30 Minutes":
            return 1800
        case "1 Hours":
            return 3600
            
        case "8 Hours":
            return 28800
            
        case "1 Day":
            return 86400
            
        case "3 Days":
            return 259200
            
        default:
            return 0
        }
        
        
    }
}

