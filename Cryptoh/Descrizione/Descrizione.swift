//
//  Descrizione.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 21/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit

class Descrizione: UIViewController {

    @IBOutlet weak var domanda: UILabel!
    @IBOutlet weak var Descr: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        Descr.text = Methods.shared.descrizione
        domanda.text = Methods.shared.titoloDescrizione
        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var gotit: UIButton!
    override func viewDidAppear(_ animated: Bool) {
        gotit.layer.cornerRadius = 7.0
        //EventManager.shared.trigger(eventName: "Descr")
    }
    

    @IBAction func btn(_ sender: Any) {
        if !Methods.shared.finito{
        EventManager.shared.trigger(eventName: "Descr")
        }else{
            self.dismiss(animated: true, completion: {
                Methods.shared.finito = false
            })
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
