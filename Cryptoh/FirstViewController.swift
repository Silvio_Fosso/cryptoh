//
//  FirstViewController.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 18/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit
import BWWalkthrough
import UserNotifications
import UICircularProgressRing
class FirstViewController: UIViewController,BWWalkthroughViewControllerDelegate,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AC") as! Academy1
            setlb(lb: cell.lb, circle: cell.circle)
            if Methods.shared.bia == 0 && Methods.shared.arch == 0{
                cell.circle.isHidden = true
                cell.lb1.isHidden = false
                cell.view1.backgroundColor = UIColor.init(hexString: "#34cd2b")
                cell.learn.backgroundColor = UIColor.init(hexString: "#22b81a")
                cell.learn.setTitle("Learn", for: .normal)
                
            }else
            {
              cell.circle.isHidden = false
                cell.lb1.isHidden = true
                // view1.backgroundColor = UIColor.init(hexString: "#D75F00")
                cell.view1.backgroundColor = #colorLiteral(red: 1, green: 0.6535633206, blue: 0.4702709317, alpha: 1)
                cell.learn.backgroundColor = #colorLiteral(red: 0.99385041, green: 0.4715980887, blue: 0.1631038189, alpha: 1)
                cell.learn.setTitle("Continue", for: .normal)
                
            }
            
            EventManager.shared.listenTo(eventName: "aumenta") {
                cell.circle.isHidden = false
                cell.lb1.isHidden = true
                if !Methods.shared.FINALE{
                    if cell.circle.value+25 >= 100{
                        cell.circle.value = 0
                        self.setlb(lb: cell.lb,circle: cell.circle)
                    }
                    
                    cell.circle.startProgress(to: cell.circle.value+25.0, duration: 2.0){
                        
                    }
                }
                
                
            }
            
            EventManager.shared.listenTo(eventName: "vfg") {
                cell.circle.isHidden = false
                cell.lb1.isHidden = true
                // view1.backgroundColor = UIColor.init(hexString: "#D75F00")
                cell.view1.backgroundColor = #colorLiteral(red: 1, green: 0.6535633206, blue: 0.4702709317, alpha: 1)
                cell.learn.backgroundColor = #colorLiteral(red: 0.99385041, green: 0.4715980887, blue: 0.1631038189, alpha: 1)
               cell.learn.setTitle("Continue", for: .normal)
            }
            
            if Methods.shared.FINALE{
                cell.circle.value = 100
            }
            
            EventManager.shared.listenTo(eventName: "ultimo") {
                cell.circle.startProgress(to: cell.circle.value+25, duration: 5.0)
            }
            cell.view1.layer.cornerRadius = 2.0
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "QZ") as! QuizAcademy1
            cell.view2.layer.cornerRadius = 2.0
            return cell
        }
        
    }
    
    @IBOutlet weak var lb: UILabel!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var quiz: UIButton!
    var walkthrough = BWWalkthroughViewController()
    
    @IBOutlet weak var circle: UICircularProgressRing!
  
    @IBOutlet weak var lb1: UILabel!
    override func viewDidLoad() {
        Methods.shared.getquizfinale()
        Methods.shared.getArchBia()
        self.tabBarController?.tabBar.items!.forEach({ (tab) in
            tab.imageInsets = UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6);
            
        })
        
        super.viewDidLoad()
       
        
        
        Methods.shared.getfinale()
        checkTrade()
        triggerBtnAcademy()
        triggerBtnquiz()
        tb.delegate = self
        tb.dataSource = self
        tb.allowsSelection = false
        tb.register(UINib(nibName: "Academy1", bundle: nil), forCellReuseIdentifier: "AC")
        tb.register(UINib(nibName: "QuizAcademy1", bundle: nil), forCellReuseIdentifier: "QZ")
        tb.frame.size.width = UIScreen.main.bounds.width
        tb.frame.size.height = UIScreen.main.bounds.height
    }
    
    @IBOutlet weak var tb: UITableView!
    
    func triggerBtnAcademy(){
        
        EventManager.shared.listenTo(eventName: "academytrigger") {
            self.learn.sendActions(for: .touchUpInside)
        }
    }
    func triggerBtnquiz(){
        
        EventManager.shared.listenTo(eventName: "quiztrigger") {
            self.quiz.sendActions(for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var learn: UIButton!
    func setlb(lb : UILabel,circle : UICircularProgressRing){
        if Methods.shared.bia == 0 {
            lb.text = "Beginner"
        }else if Methods.shared.bia == 1{
            lb.text = "Intermediate"
        }else{
            lb.text = "Advanced"
        }
        
        if Methods.shared.arch == 0{
        circle.value = CGFloat(25 * Methods.shared.arch)
        }else{
           circle.value = CGFloat(25 * Methods.shared.arch) + 25
        }
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        registerEvent()
        Methods.shared.getArchBia()
        if !Methods.shared.flaginizio{
        self.tabBarController?.selectedIndex = 1
            Methods.shared.flaginizio = true
        }
              
         let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.requestAuthorization(options: .alert) { (granted, error) in
            if(error != nil) {
                print("Se c'è un errore, fai qui qualcosa.")
            }
        }
         
        
    }
    
    func ultimoSforzo(){
        
        
    }
    @IBAction func quiz(_ sender: Any) {
          performSegue(withIdentifier: "Quiz", sender: nil)
    }
    
    func checkTrade(){
        if  UserDefaults.standard.bool(forKey: "attivo") != false{
            
          let interval =  UserDefaults.standard.value(forKey: "orario") as! TimeInterval
           let now = Date().timeIntervalSince1970
            if now < interval {
                let tempoRimasto = secondsToHoursMinutesSeconds(seconds: Int(interval-now))
                print(tempoRimasto)
                Methods.shared.timer(dati: tempoRimasto)
            }else{
                UserDefaults.standard.set(false, forKey: "attivo")
                if UserDefaults.standard.value(forKey: "valore") as! Double != 0{
                Request.shared.getbitcoinNow { (quindi) in
                    var buy = quindi["EUR"]?.buy
                    let soldoni = buy! - (UserDefaults.standard.value(forKey: "valore") as! Double) 
                    
                    if soldoni >= 0{
                        let eur = Double(Methods.shared.getEuro())
                        let gv = UserDefaults.standard.value(forKey: "soldi1") as! Double
                        UserDefaults.standard.set(String(eur!+soldoni+gv), forKey: "eur")
                        var arrayNot = UserDefaults.standard.value(forKey: "Notifiche") as! Array<String>
                        arrayNot.append(String(soldoni))
                        DispatchQueue.main.sync {
                            EventManager.shared.trigger(eventName: "reload")
                            EventManager.shared.trigger(eventName: "cambia1")
                        }
                        
                        UserDefaults.standard.set(arrayNot, forKey: "Notifiche")
                        
                    }else{
                        let eur = Double(Methods.shared.getEuro())
                        UserDefaults.standard.set(String(eur!-soldoni), forKey: "eur")
                        var arrayNot = UserDefaults.standard.value(forKey: "Notifiche") as! Array<String>
                        arrayNot.append(String(soldoni))
                        DispatchQueue.main.sync {
                            EventManager.shared.trigger(eventName: "reload")
                            EventManager.shared.trigger(eventName: "cambia1")
                        }
                       UserDefaults.standard.set(arrayNot, forKey: "Notifiche")
                    }
                    UserDefaults.standard.set(0, forKey: "valore")
                   
                }
                }
                
            }
        }
        
    }

    @IBAction func btn(_ sender: Any) {
        if UserDefaults.standard.value(forKey: "tutorial") == nil{
            UserDefaults.standard.set(1, forKey: "tutorial")
        let stb = UIStoryboard(name: "Academy", bundle: nil)
         walkthrough = stb.instantiateViewController(withIdentifier: "Master") as! BWWalkthroughViewController
        
        let page_zero = stb.instantiateViewController(withIdentifier: "0") as UIViewController
        let page_one = stb.instantiateViewController(withIdentifier: "1") as UIViewController
        let page_two = stb.instantiateViewController(withIdentifier: "2") as UIViewController
         let page_three = stb.instantiateViewController(withIdentifier: "3") as UIViewController
         let page_four = stb.instantiateViewController(withIdentifier: "4") as UIViewController
        
        walkthrough.delegate = self
            walkthrough.add(viewController: page_zero)
        walkthrough.add(viewController: page_one)
        walkthrough.add(viewController: page_two)
        walkthrough.add(viewController: page_three)
        walkthrough.add(viewController: page_four)
        walkthrough.closeButton?.layer.cornerRadius = 7.0
        self.present(walkthrough,animated: true,completion: nil)
        }else{
            performSegue(withIdentifier: "Pg", sender: nil)
        }
    }
    
    func walkthroughPageDidChange(_ pageNumber: Int) {
        if(pageNumber == 4){
            walkthrough.closeButton?.isHidden = false
        }else{
            walkthrough.closeButton?.isHidden = true
        }
    }
    func walkthroughCloseButtonPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    
    func registerEvent(){
        
        NotificationCenter.default.addObserver(
            self, selector: #selector(weAreActive(notification:)),
            name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    @objc func weAreActive(notification: NSNotification) {
        Methods.shared.timer.invalidate()
      checkTrade()
    }
    
}
@IBDesignable extension UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}
