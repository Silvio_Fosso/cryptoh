//
//  15m.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 27/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import Foundation
import Foundation

// MARK: - WelcomeValue
class quindicim: Codable {
    let the15M, last, buy, sell: Double?
    let symbol: String?
    
    enum CodingKeys: String, CodingKey {
        case the15M = "15m"
        case last, buy, sell, symbol
    }
    
    init(the15M: Double?, last: Double?, buy: Double?, sell: Double?, symbol: String?) {
        self.the15M = the15M
        self.last = last
        self.buy = buy
        self.sell = sell
        self.symbol = symbol
    }
}
typealias Welcome = [String: quindicim]


