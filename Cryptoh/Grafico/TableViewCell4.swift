//
//  TableViewCell4.swift
//  Cryptoh
//
//  Created by Silvio Fosso on 19/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit
import HITChartSwift
class TableViewCell4: UITableViewCell {
    private var data = [Dati]()
    var ris = [String]()
    // var sb = [String]()
    // (date: "2018/02/28", close: 10315.00, change:   (8643.076636538219-8607.486642169646)/100)
    
    
   
    @IBOutlet weak var view: UIView!
    
    private var absMaxPercentage = 0
    private var dates = [Date]()
    private var titles = [String]()
    var xValue: Double = 8
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
       
    }
    @IBOutlet weak var chart: HITLineChartView!
    @IBOutlet weak var ex: UILabel!
    @IBOutlet weak var cv: UIView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        cv.isUserInteractionEnabled = true
        
        Request.shared.getDati(completion:  {it in
            var cont = 0;
            self.dates.removeAll()
            self.titles.removeAll()
            self.ris.removeAll()
            self.data.removeAll()
            
            for i in 0...(it?.prices.count)!-1{
                if(cont != 0){
                    var db = Date(milliseconds: Int64((it?.prices[i][0])!)) // "Dec 31, 1969, 4:00 PM" (PDT variant of 1970 UTC)
                    print(db)
                    let today = Date()
                    let formatter1 = DateFormatter()
                    formatter1.dateStyle = .full
                    formatter1.dateFormat = "HH:mm:ss"
                    formatter1.timeZone = TimeZone(abbreviation: "UTC+1:00")
                    var ps = formatter1.string(from: db)
                    print(ps)
                    
                    self.data.append(Dati(data: db, val: (it?.prices[i][1])!, change:Double(Float((it?.prices[i][1])! - (it?.prices[i-1][1])!)),or: String(ps)))
                
                    
                }else{
                    
                }
                cont+=1
            }
            
            
            DispatchQueue.main.sync {
                self.formatData()
            }
            
        })
        // Configure the view for the selected state
    }
    
    @IBOutlet weak var lb: UILabel!
    private func formatData() {
        /// absMaxPercentage
        let maxChange = abs(data.map{ $0.change }.max() ?? 0.0).rounded(.up)
        let minChange = abs(data.map{ $0.change }.min() ?? 0.0).rounded(.up)
        absMaxPercentage = Int(maxChange > minChange ? maxChange : minChange)
        
        /// date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+9:00")
        
        dates = data.map { return $0.data }
        
        let today = Date()
        let formatter1 = DateFormatter()
        formatter1.dateStyle = .medium
        ris = data.map { return formatter1.string(from: $0.data) }
        
        titles = data.map { "BTC/EUR closing: \(Double(round(10000*$0.valore)/10000))\t\($0.orario)" }
        
      
        tapLineChart(self)
    }
    @IBAction func tapLineChart(_ sender: Any) {
        
       
       
        // let max = String((data.map{ $0.close }.max() ?? 0.0).rounded(.up))
        //let min = String((data.map{ $0.close }.min() ?? 0.0).rounded(.down))
        
        //view.removeFromSuperview()
        
        self.chart.fillSuperView = true
        print(titles.count)
        self.chart.draw(absMaxPercentage,
                   
                   values: data.map{ $0.change },
                   label: (max: "", center: "", min: ""),
                   dates: dates,
                   titles: titles)
        
        
        EventManager.shared.trigger(eventName: "pinco", information: data.last?.valore)
        Methods.shared.and = data.last?.valore
       
        
        self.layoutIfNeeded()
        self.layoutMarginsDidChange()
        
        // addCloseEvent(chart)
        
    }
   
}
extension UIView{
    var fillSuperView: Bool {
        get {
            return frame == superview?.bounds && autoresizingMask == .flexibleHeight && autoresizingMask == .flexibleWidth
        }
        
        set {
            if newValue == fillSuperView { return }
            if newValue == false {
                autoresizingMask = []
            }
            else{
                translatesAutoresizingMaskIntoConstraints = true
                frame = superview!.bounds
                
                autoresizingMask = [.flexibleHeight, .flexibleWidth]
            }
        }
    }
    
}
