//
//  Coable.swift
//  Cripto
//
//  Created by Silvio Fosso on 14/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//


import Foundation

// MARK: - Welcome
struct Coda: Codable {
    let prices: [[Double]]
    
    enum CodingKeys: String, CodingKey {
        case prices
      
    }
}
