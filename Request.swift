//
//  Request.swift
//  Cripto
//
//  Created by Silvio Fosso on 14/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import Foundation
class Request{
    static let shared = Request()
    
    
    func getDati(completion : @escaping(Coda?) -> Void){
        let url = URL(string: "https://api.coingecko.com/api/v3/coins/bitcoin/market_chart?vs_currency=eur&days=1")!
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            let welcome = try? JSONDecoder().decode(Coda.self, from: data)
            completion(welcome)
        }
        
        task.resume()
        
        
    }
    
    func getbitcoinNow(completion : @escaping([String : quindicim]) -> Void){
        let url = URL(string: "https://blockchain.info/ticker")!
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data1 = data else { return }
            let welcome = try? JSONDecoder().decode(Welcome.self, from: data1)
            completion(welcome!)
            
        }
        
        task.resume()
        
        
    }
    
    
}
