//
//  Dati.swift
//  Cripto
//
//  Created by Silvio Fosso on 14/05/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import Foundation
class Dati{
    let data : Date
    let valore : Double
    let change : Double
    let orario : String
    init(data : Date,val : Double,change : Double,or : String) {
        self.data = data
        self.valore = val
        self.change = change
        self.orario = or
    }
    
}
